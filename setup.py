from setuptools import setup

setup(
    name='pypyrograph',
    version='1.0.1',
    description='class to generate customized letters in LaTeX',
    url='https://gitlab.msu.edu/browned/pypyrograph',
    author='Edward Brown',
    author_email='browned@msu.edu',
    license='MIT',
    packages=['pypyrograph'],
    entry_points = {
        'console_scripts': ['pypyrograph=pypyrograph.api:main']},
    zip_safe=False
)
