"""
parses csv file and constructs a latex letter from a base letter with keyword substitution.

Ed Brown, Michigan State University (2012, 2015, 2016)
"""

def main():
    """Entry point for the script "pypyrograph"."""

    import argparse
    from os import getenv
    from pypyrograph import Pypyrograph

    # default template
    default_template_file = 'letter_template.tex'

    parser = argparse.ArgumentParser()
    parser.add_argument("-t","--template-file",help="latex file containing template for letter structure. The template file can also be specified using the MLP_TEMPLATE environment variable. If no file is specified, will look in local directory for a file letter_template.tex",default=None)
    parser.add_argument("-b","--base-file",help="latex file containing the body of the letter",default='base_letter.tex')
    parser.add_argument("-a","--address-file",help="csv file containing the customization information for each letter",default='addr.csv')
    parser.add_argument("-x","--xelatex",action="store_true",default=False, help="process each letter with XeLaTeX. XeLaTeX is run in batchmode.")

    args = parser.parse_args()

    # After parsing the arguments, the first step is to determine the letter 
    # template.  First use the argument, then the environment variable 
    # MLP_TEMPLATE, and finally fall back to the default.
    #
    # We did this with the idea that a single template might be used for many 
    # different letters.
    
    if args.template_file:
        template_file = args.template_file
    elif getenv('MLP_TEMPLATE'):
        template_file = getenv('MLP_TEMPLATE')
    else:
        template_file = default_template_file

    # set the base letter and the address file
    base_file = args.base_file
    address_file = args.address_file

    letter = Pypyrograph(template_file,base_file)
    letter.batch_process(address_file,xelatex=args.xelatex)
