from __future__ import print_function
import subprocess
import re
import warnings
import csv

def _with_eol(s):
    return s+r'\\'+'\n'

class Default(dict):
    def __missing__(self,key):
        return key

class Pypyrograph:

    _predefined_keys = ('finished','fileID','Name','Title','FirstName','Surname','Postnominal','Institution',
        'Address','Street1','Street2','City','State','Post','Country')
    
    def __init__(self,*args,**kwargs):

        self._letter_keys ={ rk:'' for rk in self._predefined_keys }
        # regular expressions
        # address placeholder
        self._pa = re.compile(r'%! address')
        # body placeholder
        self._pb = re.compile(r'%! body')
        # opening placeholder
        self._po = re.compile(r'\\opening\{.*\}$')
        # # keyword match
        # self._pk = re.compile(r'\[\[(\S+)\]\]')
        
        self._letter_template = []
        
        _template_file = _base_letter = None
        
        if len(args) == 1:
            _template_file, = args
        elif len(args) == 2:
            _template_file, _base_letter = args
        elif len(args) == 3:
            _template_file, _base_letter = args[0:2]
            if isinstance(args[2],dict):
                self._letter_keys = args[2]
        for k,v in kwargs.items():
            self._letter_keys[k] = v        
        
        if _template_file is not None:
            try:
                self.set_letter_template(_template_file)
            except:
                warnings.warn('unable to open letter template')
                self._letter_template = []
        if _base_letter is not None:
            try:
                self.set_base(_base_letter)
            except:
                warnings.warn('unable to open base letter')
                self._base_letter = ""

    def set_letter_keys(self,*args,**kwargs):
        if len(args) > 0 and isinstance(args[0],dict):
            for k,v in args[0].items():
                self._letter_keys[k] = v
        for k,v in kwargs.items():
            self._letter_keys[k] = v
    
    def set_letter_template(self,template_file):
        with open(template_file,'r') as tf:
            for line in tf:
                self._letter_template.append(line)
        return self
        
    def set_base(self,base_file):
        with open(base_file,'r') as bf:
            self._base_letter = bf.read()
        return self

    def finished(self):
        return self._letter_keys['finished']
    
    def fileID(self):
        return self._letter_keys['fileID']

    def _substitute_keyword(self,regex_match):
        """
        Arguments:
            regex_match := a regex match object
        """
        key = regex_match.group(1)
        val = r'\\textbf{'+str(key)+'}'
        if key in self._letter_keys:
            if self._letter_keys[key]:
                val = self._letter_keys[key]
        return val
    
    def build_salutation(self, greeting='Dear',punctuation=':'):
        salutation = None
        row = self._letter_keys
        
        if row['Name']:
            salutation = greeting+row['Name']+punctuation
        elif (row['Title'] and row['Surname']):
            salutation = ' '.join((greeting,row['Title'],row['Surname']))+punctuation
        return salutation

    def build_address(self):
        """
        Builds the address, constructed from the information in row.
    
        Returns:
            address := array of lines containing the postal address
            order:  Name, else Title, Surname, Postnominal; then
                    Institution; then
                    Address (complete block), else
                    Street1, Street2, City, State, Post;
                    Country
        """
    
        address = []
        row = self._letter_keys
        firstline = ""
        if row['Name']:
            firstline = row['Name']
        elif (row['Title'] and row['Surname']):
            firstline = row['Title']+' '+row['Surname']
        
        if firstline:
            if (row['Postnominal']):
                firstline += ", "+row['Postnominal']
            address.append(_with_eol(firstline))
        
        # now construct the address
        if (row['Institution']):
            address.append(_with_eol(row['Institution']))
        if row['Address']:
            address.append(row['Address'])
        else:
            for l in '12':
                a = 'Street'+l
                if row[a]:
                    address.append(_with_eol(row[a]))
            if row['City'] or row['Post']:
                line = ""
                if row['City']:
                    line = row['City']
                    if row['State']:
                        line += ', '+row['State']
                if row['Post']:
                    line += ' '+row['Post']
                address.append(_with_eol(line))
            if (row['Country']):
                address.append(_with_eol(row['Country']))
        return address

    def build_body(self,base_letter_file=None):
        """
        Opens the base letter file, and scans through it looking for
        expressions of the form [[keyword]].
        These expressions are then replaced with the entry from the dictionary 
        kw.
    
        Arguments:
            base_letter_file
                the name of the file containing the body of the letter.
        """
        body = ""
        if base_letter_file is not None:
            self.set_base(base_letter_file)
        body = self._base_letter.format_map(Default(self._letter_keys))
        return body

    def scribe_letter(self):
        letter = []
        address = self.build_address()
        salutation = self.build_salutation()
        for line in self._letter_template:
            if self._pa.match(line):
                letter += address
            elif self._po.match(line) and salutation:
                letter.append(r'\opening{'+salutation+'}\n')
            elif self._pb.match(line):
                letter += self.build_body()
            else:
                letter.append(line)
        return letter

    def write_letter(self,lettername=None,xelatex=False):
        if lettername is None:
            lettername = self.fileID()
        texname = lettername+'.tex'
        latex_cmd = [ 'xelatex', '-interaction=batchmode', lettername ]
        
        with open(texname,'w') as tex:
            for line in self.scribe_letter():
                tex.write(line)

        if xelatex:
            try:
                subprocess.call(latex_cmd)
            except:
                print('unable to xelatex document')
                
    def batch_process(self,addressfile,**kwargs):
        with open(addressfile,'rU') as addresses:
            r = csv.DictReader(addresses)
            letter_count = 0
            for row in r:
                # reset all keys first
                self._letter_keys ={ rk:'' for rk in self._predefined_keys }
                self.set_letter_keys(row)
                if not self.finished():
                    # we have an unfinished letter, so construct a filename
                    letter_count += 1
                    if (not self.fileID()):
                        self.set_letter_keys(file_id='letter'+str(letter_count))
                    self.write_letter(**kwargs)

