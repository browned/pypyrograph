pypyrograph: from *papyrograph*, a device for making copies.

This is a simple python package for generating letters with keyword replacement.
To generate the MSU letterhead, you will need the `msuletter` LaTeX class, 
which you can obtain from 
[MSU gitlab](https://gitlab.msu.edu/browned/msu-latex-letterhead).

Instructions
------------

## Prerequisites

A standard Python (v2.7+) installation, e.g., Anaconda.  You will need the 
`pip` installer.  

## Installation

1. From within the top-level directory (containing `setup.py`), execute

        pip install .

    This will install the pypyrograph class files into your distribution and make a command-line tool, `pypyrograph`.
    
2. Test the installation

        cd tests
        ./build_sample
        
    If you have `xelatex` and the `msuletter` LaTeX class installed, you can 
    generate the pdf's with `./build_sample -x`.

## Usage

To use the script, you need three files:
    
1.  A template that contains the basic layout of the letter.  All of the changes happen in the lines

        \begin{letter}{%
        %! address
        }
        \opening{Dear Colleagues:}
        %! body
        
    The lines `%! address` and `%! body` will be replaced for each letter generated.
    
2.  A file containing the body of the letter.  The text may contain keywords of the form `[[keyword]]`; for each letter generated these keywords are replaced by a corresponding entry from the csv spreadsheet (see next item).

3.  A csv file.  Each row of the csv file corresponds to a different letter. The columns are arbitrary, but there are a few that are required:

   * `finished`: if non-blank, the row is skipped
   * `file_id`: a desired name for the tex and pdf files.  If blank, the names 
     are letter1.tex, letter2.tex, etc.
   * `Title, Name, Surname`: To whom the letter is addressed. If `Title` and 
     `Surname` are present, the name of the addressee is included in the 
     address and the opening is changed to `Dear Title Surname`.
   * `Institution`: Not used in the address, but available as a keyword 
     throughout the letter.
   *  `Address1, Address2, Address3, Address4, Address5`: Up to five address 
     lines.  If you need more, you can always write Address5 as
             `line 1\\ line 2`. 
   *  `Country`: If present, will be added to the address block.

   When parsing the body of the letter, the code will attempt to replace
   `[[keyword]]` by looking for an entry in the column `keyword` for that row.
   Note that keyword can be a word, a phrase, *or a tex-directive such as
   `\input{other-file}`. This allows one to customize a base letter by pulling
   in different paragraphs.
   
For applications such as letters to graduate school, you could even give the
applicant a spreadsheet for the address information and then use that as a
basis for the csv file.
   
Please look at `tests/build_sample` for an example of how this works&mdash; and 
write better letters than this example!